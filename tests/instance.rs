use {
    core::{iter::once, str::FromStr},
    fenrir_hal::{instance::Metadata, Instance},
    itertools::Itertools,
    sleipnir::{ffi::runtime::Handle as FfiHandle, runtime::Handle},
    std::{env::var, fs::read_dir, path::PathBuf},
    tokio::test,
    tracing::{error, level_filters::LevelFilter},
    tracing_subscriber::fmt::format::FmtSpan,
};

#[inline]
fn get_allocator_manager(
    allocator_backend: &fenrir_hal::allocator::Backend,
    runtime: &FfiHandle,
) -> fenrir_hal::allocator::manager::Manager {
    let mainifest_dir_result = var("CARGO_MANIFEST_DIR");

    assert!(mainifest_dir_result.is_ok());

    let mut library_dir = mainifest_dir_result.unwrap();

    library_dir.push_str("/target/");

    library_dir.push_str(mode());

    library_dir.push_str("/deps");

    let maybe_entires = read_dir(&library_dir);

    assert!(maybe_entires.is_ok());

    let name = match allocator_backend {
        fenrir_hal::allocator::Backend::GPUAllocator => "gpu_allocator",
    };

    let maybe_allocator_manager = maybe_entires
        .unwrap()
        .filter_map(|entry_result| entry_result.ok())
        .filter(|entry| entry.file_name().to_string_lossy().contains(name))
        .filter_map(|entry| {
            match fenrir_hal::allocator::manager::Manager::new(&entry.path(), runtime.clone()) {
                Err(error) => {
                    error!("{entry:?}: {error}");
                    None
                }
                Ok(module) => Some(module),
            }
        })
        .find(|_| true);

    assert!(maybe_allocator_manager.is_some());

    maybe_allocator_manager.unwrap()
}

#[inline]
fn init_tracing() {
    assert!(tracing_subscriber::fmt()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_max_level(LevelFilter::TRACE)
        .try_init()
        .is_ok());
}

#[inline]
fn load_instance(
    info: (
        fenrir_hal::allocator::Backend,
        fenrir_hal::instance::Backend,
        &str,
    ),
) -> Instance {
    let (allocator_backend, instance_backend, name) = info;

    let metadata = Metadata {
        name: name.into(),
        major: 0,
        minor: 0,
        patch: 0,
        tracing_level: 0,
        shader_compiler: Vec::new().into(),
    };

    let runtime = Handle::current().into();

    let allocator_manager = get_allocator_manager(&allocator_backend, &runtime);

    let mainifest_dir_result = var("CARGO_MANIFEST_DIR");

    assert!(mainifest_dir_result.is_ok());

    let library_dir_result = PathBuf::from_str(&mainifest_dir_result.unwrap());

    assert!(library_dir_result.is_ok());

    let mut library_dir = library_dir_result.unwrap();

    library_dir.push("target");

    library_dir.push(mode());

    library_dir.push("deps");

    let maybe_entires = read_dir(&library_dir);

    assert!(maybe_entires.is_ok());

    let instance_name = match instance_backend {
        fenrir_hal::instance::Backend::Ash => "fenrir_ash",
    };

    let metadata_ref = &metadata;
    let runtime_ref = &runtime;
    let allocator_manager_ref = &allocator_manager;

    let maybe_instance = maybe_entires
        .unwrap()
        .filter_map(|entry_result| entry_result.ok())
        .filter(|entry| entry.file_name().to_string_lossy().contains(instance_name))
        .filter_map(|entry| {
            let instance_result = Instance::new(
                &entry.path(),
                metadata_ref.clone(),
                runtime_ref.clone(),
                allocator_manager_ref.clone(),
            );

            match instance_result {
                Err(error) => {
                    error!("{entry:?}: {error}");
                    None
                }
                Ok(instance) => Some(instance),
            }
        })
        .find(|_| true);

    assert!(maybe_instance.is_some());

    maybe_instance.unwrap()
}

#[inline]
fn load_instances(instance_name: &str) -> Vec<Instance> {
    once(fenrir_hal::allocator::Backend::GPUAllocator)
        .cartesian_product([fenrir_hal::instance::Backend::Ash])
        .map(|(allocator_backend, instance_backend)| {
            (allocator_backend, instance_backend, instance_name)
        })
        .map(load_instance)
        .collect()
}

#[inline]
#[cfg(debug_assertions)]
const fn mode() -> &'static str {
    "debug"
}

#[inline]
#[cfg(not(debug_assertions))]
const fn mode() -> &'static str {
    "release"
}

#[test(flavor = "multi_thread", worker_threads = 1)]
async fn new() {
    init_tracing();

    let _instances = load_instances("backend::tests::new");
}
