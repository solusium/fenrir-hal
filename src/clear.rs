use {crate::color::RGBA, abi_stable::StableAbi, float_eq::float_eq};

#[repr(C)]
#[derive(Clone, Copy, Debug, StableAbi)]
pub enum Value {
    FColor(RGBA<f32>),
    SColor(RGBA<isize>),
    UColor(RGBA<usize>),
    DepthStencil { depth: f32, stencil: usize },
}

impl Eq for Value {}

impl PartialEq for Value {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::FColor(color), Self::FColor(other_color)) => color == other_color,
            (Self::SColor(color), Self::SColor(other_color)) => color == other_color,
            (Self::UColor(color), Self::UColor(other_color)) => color == other_color,
            (
                Self::DepthStencil { depth, stencil },
                Self::DepthStencil {
                    depth: other_depth,
                    stencil: other_stencil,
                },
            ) => float_eq!(depth, other_depth, r2nd <= f32::EPSILON) && (stencil == other_stencil),
            _ => false,
        }
    }
}
