use {abi_stable::StableAbi, core::f32::EPSILON, float_eq::float_eq};

#[repr(C)]
#[derive(Clone, Copy, Debug, StableAbi)]
pub struct Bias {
    enabled: bool,
    clamp: f32,
    constant_factor: f32,
    slope_factor: f32,
}

impl Bias {
    #[inline]
    #[must_use]
    pub const fn new(enabled: bool, clamp: f32, constant_factor: f32, slope_factor: f32) -> Self {
        Self {
            enabled,
            clamp,
            constant_factor,
            slope_factor,
        }
    }

    #[inline]
    #[must_use]
    pub const fn clamp(&self) -> f32 {
        self.clamp
    }

    #[inline]
    #[must_use]
    pub const fn constant_factor(&self) -> f32 {
        self.constant_factor
    }

    #[inline]
    #[must_use]
    pub const fn enabled(&self) -> bool {
        self.enabled
    }

    #[inline]
    pub fn set_clamp(&mut self, clamp: f32) {
        self.clamp = clamp;
    }

    #[inline]
    pub fn set_constant_factor(&mut self, constant_factor: f32) {
        self.constant_factor = constant_factor;
    }

    #[inline]
    pub fn set_enabled(&mut self, enabled: bool) {
        self.enabled = enabled;
    }

    #[inline]
    pub fn set_slope_factor(&mut self, slope_factor: f32) {
        self.slope_factor = slope_factor;
    }

    #[inline]
    #[must_use]
    pub const fn slope_factor(&self) -> f32 {
        self.slope_factor
    }
}

impl Default for Bias {
    #[inline]
    fn default() -> Self {
        Self {
            enabled: false,
            clamp: 0.0,
            constant_factor: 0.0,
            slope_factor: 0.0,
        }
    }
}

impl Eq for Bias {}

impl PartialEq for Bias {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.enabled == other.enabled
            && float_eq!(self.clamp, other.clamp, r2nd <= EPSILON)
            && float_eq!(self.constant_factor, other.constant_factor, r2nd <= EPSILON)
            && float_eq!(self.slope_factor, other.slope_factor, r2nd <= EPSILON)
    }
}
