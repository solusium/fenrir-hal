#[cfg(feature = "enum-derived")]
use enum_derived::Rand;
use {
    crate::{color::component::Mapping, device::Device, texture::stable_abi::Trait_TO},
    abi_stable::{
        std_types::{
            RArc,
            ROption::{self, RNone, RSome},
            RStr, RString, RVec,
        },
        StableAbi,
    },
    core::fmt::Debug,
    fenrir_error::Error,
    itertools::Itertools,
    smallvec::SmallVec,
    strum::{EnumCount, EnumIter},
};

pub mod guard;
pub mod view;

pub type Guard = crate::texture::guard::Guard;
pub type View = crate::texture::view::View;

pub mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::non_send_fields_in_send_ty,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {
        crate::{
            color::component::Mapping,
            texture::{guard::Guard, view::View, Metadata, SubresourceRange},
        },
        abi_stable::{sabi_trait, std_types::RResult},
        fenrir_error::Error,
    };

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub trait Trait: Clone + Debug + Eq + PartialEq + Send + Sync + 'static {
        fn guard(&self) -> Guard;

        fn metadata(&self) -> Metadata;

        fn view(&self, components: Mapping, subresource: SubresourceRange) -> RResult<View, Error>;
    }
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, StableAbi)]
#[cfg_attr(feature = "enum-derived", derive(Rand))]
pub enum Aspect {
    None,
    Color,
    Depth,
    Stencil,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, StableAbi)]
#[cfg_attr(feature = "enum-derived", derive(Rand))]
pub enum Type {
    OneDimensional,
    TwoDimensional,
    ThreeDimensional,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, EnumCount, EnumIter, StableAbi)]
#[cfg_attr(feature = "enum-derived", derive(Rand))]
pub enum Usage {
    ColorAttachment,
    DepthStencilAttachment,
    Destination,
    InputAttachment,
    Sampled,
    Source,
    Storage,
    TransientAttachment,
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Metadata {
    device: Device,
    width: usize,
    height: usize,
    depth: usize,
    usage: RVec<Usage>,
    name: RString,
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct MutateMetadata {
    pub depth: ROption<usize>,
    pub height: ROption<usize>,
    pub width: ROption<usize>,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub struct SubresourceRange {
    aspects: [ROption<Aspect>; 4],
    base_mip_level: usize,
    level_count: usize,
    base_array_layer: usize,
    layer_count: usize,
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct Texture {
    inner: Trait_TO<RArc<()>>,
}

impl Metadata {
    #[inline]
    #[must_use]
    pub fn new(
        device: Device,
        width: usize,
        height: usize,
        depth: usize,
        usage: Vec<Usage>,
        name: String,
    ) -> Self {
        Self {
            device,
            width,
            height,
            depth,
            usage: usage.into(),
            name: name.into(),
        }
    }

    #[inline]
    #[must_use]
    pub const fn depth(&self) -> usize {
        self.depth
    }

    #[inline]
    #[must_use]
    pub fn device(&self) -> Device {
        self.device.clone()
    }

    #[inline]
    #[must_use]
    pub const fn height(&self) -> usize {
        self.height
    }

    #[inline]
    #[must_use]
    pub const fn name(&self) -> RStr {
        self.name.as_rstr()
    }

    #[inline]
    pub fn set_depth(&mut self, depth: usize) {
        self.depth = depth;
    }

    #[inline]
    pub fn set_height(&mut self, height: usize) {
        self.height = height;
    }

    #[inline]
    pub fn set_name(&mut self, name: String) {
        self.name = name.into();
    }

    #[inline]
    pub fn set_usage(&mut self, usage: impl IntoIterator<Item = Usage>) {
        self.usage = usage.into_iter().collect();
    }

    #[inline]
    pub fn set_width(&mut self, width: usize) {
        self.width = width;
    }

    #[inline]
    #[must_use]
    pub const fn width(&self) -> usize {
        self.width
    }

    #[inline]
    #[must_use]
    pub fn usage(&self) -> &[Usage] {
        &self.usage
    }
}

impl SubresourceRange {
    #[inline]
    #[must_use]
    pub fn new(
        aspects: &[Aspect],
        base_mip_level: usize,
        level_count: usize,
        base_array_layer: usize,
        layer_count: usize,
    ) -> Self {
        let mut maybe_aspects = [RNone; 4];

        aspects
            .iter()
            .unique()
            .enumerate()
            .for_each(|(index, aspect)| {
                maybe_aspects[index] = RSome(*aspect);
            });

        let aspects = maybe_aspects;

        Self {
            aspects,
            base_mip_level,
            level_count,
            base_array_layer,
            layer_count,
        }
    }

    #[inline]
    #[must_use]
    pub fn aspects(&self) -> SmallVec<[Aspect; 4]> {
        self.aspects
            .iter()
            .filter_map(|maybe_aspect| (*maybe_aspect).into())
            .collect()
    }

    #[inline]
    #[must_use]
    pub const fn base_array_layer(&self) -> usize {
        self.base_array_layer
    }

    #[inline]
    #[must_use]
    pub const fn base_mip_level(&self) -> usize {
        self.base_mip_level
    }

    #[inline]
    #[must_use]
    pub const fn layer_count(&self) -> usize {
        self.layer_count
    }

    #[inline]
    #[must_use]
    pub const fn level_count(&self) -> usize {
        self.level_count
    }
}

impl Texture {
    #[inline]
    #[must_use]
    pub const fn new(inner: Trait_TO<RArc<()>>) -> Self {
        Self { inner }
    }

    #[inline]
    #[must_use]
    pub fn guard(&self) -> Guard {
        self.inner.guard()
    }

    #[inline]
    #[must_use]
    pub fn metadata(&self) -> Metadata {
        self.inner.metadata()
    }

    #[inline]
    #[must_use]
    pub fn inner(&self) -> Trait_TO<RArc<()>> {
        self.inner.clone()
    }

    /// Construct a view of the texture.
    ///
    /// # Errors
    ///
    /// This function will return an error if the underlying API encounters an error.
    #[inline]
    pub fn view(&self, components: Mapping, subresource: SubresourceRange) -> Result<View, Error> {
        self.inner.view(components, subresource).into()
    }
}
unsafe impl Send for Metadata {}
unsafe impl Sync for Metadata {}

unsafe impl Send for Texture {}
unsafe impl Sync for Texture {}

impl Unpin for Texture {}
