use abi_stable::StableAbi;

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Mode {
    None,
    Front,
    Back,
    FrontAndBack,
}
