use {
    crate::{
        color::Blend, cull::Mode as CullMode, depth::Bias, front::Face, geometry::Rect2D,
        polygon::Mode as PolygonMode, shader::Module, smallvec::SmallVec, vertex::Input, Depth,
        Device, Multisample, Primitive, Stencil, Viewport,
    },
    abi_stable::{std_types::RVec, StableAbi},
    core::{marker::PhantomData, num::NonZeroUsize},
};

pub struct Builder<'a, 'b, 'c> {
    device: Device,
    color_blend: Blend,
    cull_mode: CullMode,
    depth: Depth,
    depth_bias: Bias,
    depth_clamp: bool,
    front_face: Face,
    line_width: f32,
    multisample: Multisample,
    polygon_mode: PolygonMode,
    primitive: Primitive,
    rasterizer_discard: bool,
    scissors: SmallVec<Rect2D, 2>,
    shaders: RVec<Module<'a, 'b>>,
    stencil: Stencil,
    tessellation_patch_control_points: NonZeroUsize,
    viewports: SmallVec<Viewport, 2>,
    vertex_input: Input,
    _phantom_data: PhantomData<&'c ()>,
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Metadata<'a, 'b> {
    device: Device,
    color_blend: Blend,
    cull_mode: CullMode,
    depth: Depth,
    depth_bias: Bias,
    depth_clamp: bool,
    front_face: Face,
    line_width: f32,
    multisample: Multisample,
    polygon_mode: PolygonMode,
    primitive: Primitive,
    rasterizer_discard: bool,
    scissors: SmallVec<Rect2D, 2>,
    shaders: RVec<Module<'a, 'b>>,
    stencil: Stencil,
    tessellation_patch_control_points: NonZeroUsize,
    viewports: SmallVec<Viewport, 2>,
    vertex_input: Input,
}

impl<'a, 'b, 'c> Builder<'a, 'b, 'c> {
    #[inline]
    #[must_use]
    fn new(device: Device) -> Self {
        let color_blend = Blend::default();

        let cull_mode = CullMode::None;

        let depth = Depth::default();

        let depth_bias = Bias::default();

        let depth_clamp = false;

        let front_face = Face::Clockwise;

        let line_width = 1.0;

        let multisample = Multisample::default();

        let polygon_mode = PolygonMode::Fill;

        let primitive = Primitive::default();

        let rasterizer_discard = false;

        let scissors = SmallVec::new(&[]);

        let shaders = RVec::new();

        let stencil = Stencil::default();

        let tessellation_patch_control_points = unsafe { NonZeroUsize::new_unchecked(1) };

        let viewports = SmallVec::new(&[]);

        let vertex_input = Input::default();

        Self {
            device,
            color_blend,
            cull_mode,
            depth,
            depth_bias,
            depth_clamp,
            front_face,
            line_width,
            multisample,
            polygon_mode,
            primitive,
            rasterizer_discard,
            scissors,
            shaders,
            stencil,
            tessellation_patch_control_points,
            viewports,
            vertex_input,
            _phantom_data: PhantomData,
        }
    }

    #[inline]
    #[must_use]
    pub fn add_shader(mut self, shader: Module<'a, 'b>) -> Self {
        self.shaders.push(shader);

        self
    }

    #[inline]
    #[must_use]
    pub fn build(self) -> Metadata<'a, 'b> {
        Metadata {
            device: self.device,
            color_blend: self.color_blend,
            cull_mode: self.cull_mode,
            depth: self.depth,
            depth_bias: self.depth_bias,
            depth_clamp: self.depth_clamp,
            front_face: self.front_face,
            line_width: self.line_width,
            multisample: self.multisample,
            polygon_mode: self.polygon_mode,
            primitive: self.primitive,
            rasterizer_discard: self.rasterizer_discard,
            scissors: self.scissors,
            shaders: self.shaders,
            stencil: self.stencil,
            tessellation_patch_control_points: self.tessellation_patch_control_points,
            viewports: self.viewports,
            vertex_input: self.vertex_input,
        }
    }

    #[inline]
    #[must_use]
    pub fn set_color_blend(mut self, color_blend: Blend) -> Self {
        self.color_blend = color_blend;

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_cull_mode(mut self, cull_mode: CullMode) -> Self {
        self.cull_mode = cull_mode;

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_depth(mut self, depth: Depth) -> Self {
        self.depth = depth;

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_depth_bias(mut self, depth_bias: Bias) -> Self {
        self.depth_bias = depth_bias;

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_depth_clamp(mut self, depth_clamp: bool) -> Self {
        self.depth_clamp = depth_clamp;

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_front_face(mut self, front_face: Face) -> Self {
        self.front_face = front_face;

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_line_width(mut self, line_width: f32) -> Self {
        self.line_width = line_width;

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_multisample(mut self, multisample: Multisample) -> Self {
        self.multisample = multisample;

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_polygon_mode(mut self, polygon_mode: PolygonMode) -> Self {
        self.polygon_mode = polygon_mode;

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_primitive(mut self, primitive: Primitive) -> Self {
        self.primitive = primitive;

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_rasterizer_discard(mut self, rasterizer_discard: bool) -> Self {
        self.rasterizer_discard = rasterizer_discard;

        self
    }

    #[inline]
    #[must_use]
    pub fn set_scissors(mut self, scissors: &[Rect2D]) -> Self {
        self.scissors = SmallVec::new(scissors);

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_stencil(mut self, stencil: Stencil) -> Self {
        self.stencil = stencil;

        self
    }

    #[inline]
    #[must_use]
    pub const fn set_tessellation_patch_control_points(
        mut self,
        tessellation_patch_control_points: NonZeroUsize,
    ) -> Self {
        self.tessellation_patch_control_points = tessellation_patch_control_points;

        self
    }

    #[inline]
    #[must_use]
    pub fn set_viewports(mut self, viewports: &[Viewport]) -> Self {
        self.viewports = SmallVec::new(viewports);

        self
    }

    #[inline]
    #[must_use]
    pub fn set_vertex_input(mut self, vertex_input: Input) -> Self {
        self.vertex_input = vertex_input;

        self
    }
}

impl<'a, 'b> Metadata<'a, 'b> {
    #[inline]
    #[must_use]
    pub fn builder<'c>(device: Device) -> Builder<'a, 'b, 'c> {
        Builder::new(device)
    }

    #[inline]
    #[must_use]
    pub const fn color_blend(&self) -> &Blend {
        &self.color_blend
    }

    #[inline]
    #[must_use]
    pub const fn cull_mode(&self) -> CullMode {
        self.cull_mode
    }

    #[inline]
    #[must_use]
    pub const fn depth(&self) -> &Depth {
        &self.depth
    }

    #[inline]
    #[must_use]
    pub const fn depth_bias(&self) -> &Bias {
        &self.depth_bias
    }

    #[inline]
    #[must_use]
    pub const fn depth_clamp(&self) -> bool {
        self.depth_clamp
    }

    #[inline]
    #[must_use]
    pub const fn device(&self) -> &Device {
        &self.device
    }

    #[inline]
    #[must_use]
    pub const fn front_face(&self) -> Face {
        self.front_face
    }

    #[inline]
    #[must_use]
    pub const fn line_width(&self) -> f32 {
        self.line_width
    }

    #[inline]
    #[must_use]
    pub const fn multisample(&self) -> &Multisample {
        &self.multisample
    }

    #[inline]
    #[must_use]
    pub const fn polygon_mode(&self) -> PolygonMode {
        self.polygon_mode
    }

    #[inline]
    #[must_use]
    pub const fn primitive(&self) -> Primitive {
        self.primitive
    }

    #[inline]
    #[must_use]
    pub const fn rasterizer_discard(&self) -> bool {
        self.rasterizer_discard
    }

    #[inline]
    #[must_use]
    pub fn scissors(&self) -> &[Rect2D] {
        &self.scissors
    }

    #[inline]
    #[must_use]
    pub fn shaders(&self) -> &[Module<'a, 'b>] {
        &self.shaders
    }

    #[inline]
    #[must_use]
    pub const fn stencil(&self) -> &Stencil {
        &self.stencil
    }

    #[inline]
    #[must_use]
    pub const fn tessellation_patch_control_points(&self) -> NonZeroUsize {
        self.tessellation_patch_control_points
    }

    #[inline]
    #[must_use]
    pub fn viewports(&self) -> &[Viewport] {
        &self.viewports
    }

    #[inline]
    #[must_use]
    pub const fn vertex_input(&self) -> &Input {
        &self.vertex_input
    }
}
