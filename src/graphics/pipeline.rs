pub use crate::graphics::pipeline::metadata::Metadata;
use {
    crate::{graphics::pipeline::stable_abi::Trait_TO, RenderPass},
    abi_stable::{std_types::RArc, StableAbi},
    fenrir_error::Error,
};

pub mod metadata;

pub mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::non_send_fields_in_send_ty,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {
        crate::{graphics::pipeline::Metadata, RenderPass},
        abi_stable::{
            sabi_trait,
            std_types::{RResult, RVec},
        },
        async_ffi::BorrowingFfiFuture,
        fenrir_error::Error,
    };

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub trait Trait: Clone + Debug + Eq + PartialEq + Send + Sync + 'static {
        fn metadata(&self) -> BorrowingFfiFuture<RVec<Metadata>>;

        fn new_render_pass(
            &self,
            render_pass: RenderPass,
        ) -> BorrowingFfiFuture<RResult<(), Error>>;

        fn render_pass(&self) -> BorrowingFfiFuture<RenderPass>;
    }
}

#[repr(C)]
#[derive(Clone, Debug, PartialEq, Eq, StableAbi)]
pub struct Pipeline {
    inner: Trait_TO<RArc<()>>,
}

impl Pipeline {
    #[inline]
    #[must_use]
    pub async fn metadata(&self) -> Vec<Metadata> {
        self.inner.metadata().await.into()
    }

    #[inline]
    pub async fn new_render_pass(&self, render_pass: RenderPass) -> Result<(), Error> {
        self.inner.new_render_pass(render_pass).await.into()
    }

    #[inline]
    #[must_use]
    pub async fn render_pass(&self) -> RenderPass {
        self.inner.render_pass().await
    }
}

impl From<Trait_TO<RArc<()>>> for Pipeline {
    #[inline]
    fn from(inner: Trait_TO<RArc<()>>) -> Self {
        Self { inner }
    }
}
