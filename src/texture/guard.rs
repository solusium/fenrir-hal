use {
    crate::texture::guard::stable_abi::{Trait, Trait_TO},
    abi_stable::{sabi_trait, std_types::RArc, StableAbi},
};

pub mod stable_abi {
    #![allow(clippy::non_send_fields_in_send_ty, non_local_definitions)]

    use abi_stable::sabi_trait;

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub trait Trait: Debug + Eq + PartialEq + Send + Sync + 'static {}
}

#[repr(C)]
#[derive(Debug, Eq, PartialEq, StableAbi)]
pub struct Guard {
    inner: Trait_TO<RArc<()>>,
}

impl Guard {
    #[inline]
    #[must_use]
    pub fn new<T>(inner: RArc<T>) -> Self
    where
        T: Trait,
    {
        Self {
            inner: Trait_TO::from_ptr(inner, sabi_trait::TD_Opaque),
        }
    }
}

unsafe impl Send for Guard {}
unsafe impl Sync for Guard {}
