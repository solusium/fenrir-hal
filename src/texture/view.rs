use {
    crate::{
        color::component::Mapping,
        texture::{view::stable_abi::Trait_TO, SubresourceRange, Texture},
    },
    abi_stable::{std_types::RArc, StableAbi},
};

pub mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::non_send_fields_in_send_ty,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {crate::texture::view::Metadata, abi_stable::sabi_trait};

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub trait Trait: Clone + Debug + Eq + PartialEq + Send + Sync + 'static {
        fn metadata(&self) -> Metadata;
    }
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Metadata {
    texture: Texture,
    components: Mapping,
    subresource_range: SubresourceRange,
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct View {
    inner: Trait_TO<RArc<()>>,
}

impl Metadata {
    #[inline]
    #[must_use]
    pub const fn new(
        texture: Texture,
        components: Mapping,
        subresource_range: SubresourceRange,
    ) -> Self {
        Self {
            texture,
            components,
            subresource_range,
        }
    }

    #[inline]
    #[must_use]
    pub const fn components(&self) -> Mapping {
        self.components
    }

    #[inline]
    #[must_use]
    pub const fn subresource_range(&self) -> SubresourceRange {
        self.subresource_range
    }

    #[inline]
    #[must_use]
    pub fn texture(&self) -> Texture {
        self.texture.clone()
    }
}

impl View {
    #[inline]
    #[must_use]
    pub const fn new(inner: Trait_TO<RArc<()>>) -> Self {
        Self { inner }
    }

    #[inline]
    #[must_use]
    pub const fn inner(&self) -> &Trait_TO<RArc<()>> {
        &self.inner
    }

    #[inline]
    #[must_use]
    pub fn metadata(&self) -> Metadata {
        self.inner.metadata()
    }
}

unsafe impl Send for Metadata {}
unsafe impl Sync for Metadata {}

unsafe impl Send for View {}
unsafe impl Sync for View {}
