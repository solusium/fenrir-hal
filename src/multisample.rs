use {crate::sample::Count, abi_stable::StableAbi, float_eq::float_eq};

#[repr(C)]
#[derive(Clone, Copy, Debug, StableAbi)]
pub struct Multisample {
    rasterization_samples: Count,
    sample_shading: bool,
    min_sample_shading: f32,
    alpha_to_coverage: bool,
    alpha_to_one: bool,
}

impl Multisample {
    #[inline]
    #[must_use]
    pub const fn new(
        rasterization_samples: Count,
        sample_shading: bool,
        min_sample_shading: f32,
        alpha_to_coverage: bool,
        alpha_to_one: bool,
    ) -> Self {
        Self {
            rasterization_samples,
            sample_shading,
            min_sample_shading,
            alpha_to_coverage,
            alpha_to_one,
        }
    }

    #[inline]
    #[must_use]
    pub const fn alpha_to_one(&self) -> bool {
        self.alpha_to_one
    }

    #[inline]
    #[must_use]
    pub const fn alpha_to_coverage(&self) -> bool {
        self.alpha_to_coverage
    }

    #[inline]
    #[must_use]
    pub const fn min_sample_shading(&self) -> f32 {
        self.min_sample_shading
    }

    #[inline]
    #[must_use]
    pub const fn rasterization_samples(&self) -> Count {
        self.rasterization_samples
    }

    #[inline]
    #[must_use]
    pub const fn sample_shading(&self) -> bool {
        self.sample_shading
    }

    #[inline]
    pub fn set_alpha_to_one(&mut self, alpha_to_one: bool) {
        self.alpha_to_one = alpha_to_one;
    }

    #[inline]
    pub fn set_alpha_to_coverage(&mut self, alpha_to_coverage: bool) {
        self.alpha_to_coverage = alpha_to_coverage;
    }

    #[inline]
    pub fn set_min_sample_shading(&mut self, min_sample_shading: f32) {
        self.min_sample_shading = min_sample_shading;
    }

    #[inline]
    pub fn set_rasterization_samples(&mut self, rasterization_samples: Count) {
        self.rasterization_samples = rasterization_samples;
    }

    #[inline]
    pub fn set_sample_shading(&mut self, sample_shading: bool) {
        self.sample_shading = sample_shading;
    }
}

impl Default for Multisample {
    #[inline]
    fn default() -> Self {
        Self {
            rasterization_samples: Count::One,
            sample_shading: false,
            min_sample_shading: 1.0,
            alpha_to_coverage: false,
            alpha_to_one: false,
        }
    }
}

impl Eq for Multisample {}

impl PartialEq for Multisample {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.rasterization_samples == other.rasterization_samples
            && self.sample_shading == other.sample_shading
            && float_eq!(
                self.min_sample_shading,
                other.min_sample_shading,
                r2nd <= f32::EPSILON
            )
            && self.alpha_to_coverage == other.alpha_to_coverage
            && self.alpha_to_one == other.alpha_to_one
    }
}
