use {
    crate::{compare::Operation as CompareOperation, stencil::Operation as StencilOperation},
    abi_stable::StableAbi,
};

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub struct Operations {
    fail: StencilOperation,
    pass: StencilOperation,
    depth_fail: StencilOperation,
    compare: CompareOperation,
    compare_mask: usize,
    write_mask: usize,
    reference: usize,
}

impl Operations {
    #[inline]
    #[must_use]
    pub const fn new(
        fail: StencilOperation,
        pass: StencilOperation,
        depth_fail: StencilOperation,
        compare: CompareOperation,
        compare_mask: usize,
        write_mask: usize,
        reference: usize,
    ) -> Self {
        Self {
            fail,
            pass,
            depth_fail,
            compare,
            compare_mask,
            write_mask,
            reference,
        }
    }

    #[inline]
    #[must_use]
    pub const fn compare(&self) -> CompareOperation {
        self.compare
    }

    #[inline]
    #[must_use]
    pub const fn compare_mask(&self) -> usize {
        self.compare_mask
    }

    #[inline]
    #[must_use]
    pub const fn depth_fail(&self) -> StencilOperation {
        self.depth_fail
    }

    #[inline]
    #[must_use]
    pub const fn fail(&self) -> StencilOperation {
        self.fail
    }

    #[inline]
    #[must_use]
    pub const fn pass(&self) -> StencilOperation {
        self.pass
    }

    #[inline]
    #[must_use]
    pub const fn reference(&self) -> usize {
        self.reference
    }

    #[inline]
    pub fn set_compare(&mut self, compare: CompareOperation) {
        self.compare = compare;
    }

    #[inline]
    pub fn set_compare_mask(&mut self, compare_mask: usize) {
        self.compare_mask = compare_mask;
    }

    #[inline]
    pub fn set_depth_fail(&mut self, depth_fail: StencilOperation) {
        self.depth_fail = depth_fail;
    }

    #[inline]
    pub fn set_fail(&mut self, fail: StencilOperation) {
        self.fail = fail;
    }

    #[inline]
    pub fn set_pass(&mut self, pass: StencilOperation) {
        self.pass = pass;
    }

    #[inline]
    pub fn set_reference(&mut self, reference: usize) {
        self.reference = reference;
    }

    #[inline]
    pub fn set_write_mask(&mut self, write_mask: usize) {
        self.write_mask = write_mask;
    }

    #[inline]
    #[must_use]
    pub const fn write_mask(&self) -> usize {
        self.write_mask
    }
}

impl Default for Operations {
    fn default() -> Self {
        Self::new(
            StencilOperation::Keep,
            StencilOperation::Keep,
            StencilOperation::Keep,
            CompareOperation::Always,
            0,
            0,
            0,
        )
    }
}
