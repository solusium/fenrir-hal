use {
    crate::allocation::stable_abi::Trait_TO,
    abi_stable::{
        sabi_trait,
        std_types::{RBox, RStr},
        StableAbi,
    },
    core::slice::from_raw_parts,
};

pub mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::non_send_fields_in_send_ty,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        non_local_definitions
    )]

    use abi_stable::{
        sabi_trait,
        std_types::{ROption, Tuple2},
    };

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub trait Trait: Debug + Eq + PartialEq + Send + Sync + 'static {
        fn as_mut_ptr(&mut self) -> ROption<*mut u8>;

        fn as_ptr(&self) -> ROption<*const u8>;

        fn handle(&self) -> Tuple2<*const u8, usize>;

        fn offset(&self) -> usize;

        fn size(&self) -> usize;
    }
}

#[repr(C)]
#[derive(Debug, Eq, PartialEq, StableAbi)]
pub struct Allocation {
    state: Trait_TO<RBox<()>>,
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Metadata<'a> {
    pub requirements: Requirements,
    pub allocation_type: Type,
    pub name: RStr<'a>,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, StableAbi)]
pub struct Requirements {
    pub size: usize,
    pub alignment: usize,
    pub flags: usize,
}

#[repr(C)]
#[derive(StableAbi, Clone, Copy, Debug)]
pub enum Type {
    GpuOnly,
    CpuToGpu,
    GpuToCpu,
}

impl Allocation {
    #[inline]
    #[must_use]
    pub fn new(state: Trait_TO<RBox<()>>) -> Self {
        Self {
            state: Trait_TO::from_value(state, sabi_trait::TD_CanDowncast),
        }
    }

    #[inline]
    #[must_use]
    pub fn as_mut_ptr(&mut self) -> Option<*mut u8> {
        self.state.as_mut_ptr().into()
    }

    #[inline]
    #[must_use]
    pub fn as_ptr(&self) -> Option<*const u8> {
        self.state.as_ptr().into()
    }

    #[inline]
    #[must_use]
    pub fn handle(&self) -> &[u8] {
        let handle = self.state.handle();

        unsafe { from_raw_parts(handle.0, handle.1) }
    }

    #[inline]
    #[must_use]
    pub fn offset(&self) -> usize {
        self.state.offset()
    }

    #[inline]
    #[must_use]
    pub fn size(&self) -> usize {
        self.state.size()
    }
}
