#![allow(non_local_definitions)]

use {
    crate::{
        allocation::{Allocation, Metadata},
        allocator::stable_abi::{ModuleRef, Trait_TO},
        device,
    },
    abi_stable::{
        declare_root_module_statics,
        library::RootModule,
        package_version_strings,
        sabi_types::VersionStrings,
        std_types::{
            RArc,
            RResult::{self, RErr, ROk},
        },
        DynTrait, StableAbi,
    },
    core::fmt::{Debug, Formatter},
    fenrir_error::Error,
};

pub mod manager;

pub mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::expl_impl_clone_on_copy,
        clippy::must_use_candidate,
        clippy::non_send_fields_in_send_ty,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {
        crate::allocation::Metadata,
        abi_stable::{
            sabi_trait,
            std_types::{RArc, RBox, RResult},
            DynTrait, StableAbi,
        },
        async_ffi::BorrowingFfiFuture,
        fenrir_error::Error,
        sleipnir::ffi::runtime::Handle,
    };

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub trait Trait: Clone + Debug + Eq + PartialEq + Send + Sync + 'static {
        fn allocate<'a>(
            &self,
            metadata: Metadata<'a>,
            this: Trait_TO<RArc<()>>,
        ) -> BorrowingFfiFuture<'a, RResult<crate::allocation::stable_abi::Trait_TO<RBox<()>>, Error>>;
    }

    #[repr(C)]
    #[derive(StableAbi)]
    #[sabi(kind(Prefix(prefix_fields = ModulePrefix, prefix_ref = ModuleRef)))]
    pub struct Module {
        pub new:
            extern "C" fn(Handle) -> DynTrait<'static, RArc<()>, crate::allocator::manager::Inner>,

        #[sabi(last_prefix_field)]
        pub allocator: extern "C" fn(
            DynTrait<'static, RArc<()>, crate::allocator::manager::Inner>,
            crate::device::guard::Guard,
        ) -> RResult<Trait_TO<RArc<()>>, Error>,
    }
}

#[derive(Clone, Debug)]
pub enum Backend {
    GPUAllocator,
}

#[repr(C)]
#[derive(Clone, StableAbi)]
pub struct Allocator {
    state: Trait_TO<RArc<()>>,
    module: ModuleRef,
}

#[repr(C)]
#[derive(StableAbi)]
#[sabi(impl_InterfaceType(Clone, Debug, Send, Sync))]
pub struct State;

impl Allocator {
    #[inline]
    fn new(
        module: ModuleRef,
        state: &DynTrait<'static, RArc<()>, crate::allocator::manager::Inner>,
        device: device::guard::Guard,
    ) -> Result<Self, Error> {
        match module.allocator()(state.clone(), device) {
            RResult::ROk(state) => Ok(Self { state, module }),
            RResult::RErr(error) => Err(error),
        }
    }

    /// Allocate memory with the given metadata.
    ///
    /// # Errors
    ///
    /// This function will return an error if the API wrapped by the allocator couldn't
    /// construct the wrapped structs.
    #[inline]
    pub async fn allocate(&self, metadata: Metadata<'_>) -> Result<Allocation, Error> {
        match self.state.allocate(metadata, self.state.clone()).await {
            ROk(state) => Ok(Allocation::new(state)),
            RErr(error) => Err(error),
        }
    }
}

impl Debug for Allocator {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Allocator")
            .field("state", &self.state)
            .finish_non_exhaustive()
    }
}

impl Eq for Allocator {}

impl PartialEq for Allocator {
    fn eq(&self, other: &Self) -> bool {
        self.state == other.state
    }
}

#[allow(clippy::nursery)]
impl RootModule for ModuleRef {
    declare_root_module_statics! {ModuleRef}

    const BASE_NAME: &'static str = "fenrir_allocator";

    const NAME: &'static str = "fenrir-allocator";

    const VERSION_STRINGS: VersionStrings = package_version_strings!();
}
