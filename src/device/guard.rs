use {
    crate::device::{
        guard::stable_abi::{Trait, Trait_TO},
        Handle,
    },
    abi_stable::{sabi_trait, std_types::RArc, StableAbi},
};

pub mod stable_abi {
    #![allow(clippy::non_send_fields_in_send_ty, non_local_definitions)]

    use {crate::device::Handle, abi_stable::sabi_trait};

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub trait Trait: Clone + Debug + Eq + PartialEq + Send + Sync + 'static {
        fn handle(&self) -> Handle;
    }
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct Guard {
    inner: Trait_TO<RArc<()>>,
}

impl Guard {
    #[inline]
    #[must_use]
    pub fn new<S>(inner: RArc<S>) -> Self
    where
        S: Trait,
    {
        Self {
            inner: Trait_TO::from_ptr(inner, sabi_trait::TD_Opaque),
        }
    }

    #[inline]
    #[must_use]
    pub fn handle(&self) -> Handle {
        self.inner.handle()
    }
}

unsafe impl Send for Guard {}
unsafe impl Sync for Guard {}
