pub use crate::stencil::operations::Operations;
use abi_stable::StableAbi;

mod operations;

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Operation {
    Keep,
    Zero,
    Replace,
    IncrementAndClamp,
    DecrementAndClamp,
    Invert,
    IncrementAndWrap,
    DecrementAndWrap,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub struct Stencil {
    test: bool,
    front: Operations,
    back: Operations,
}

impl Stencil {
    #[inline]
    #[must_use]
    pub const fn new(test: bool, front: Operations, back: Operations) -> Self {
        Self { test, front, back }
    }

    #[inline]
    #[must_use]
    pub const fn back(&self) -> &Operations {
        &self.back
    }

    #[inline]
    #[must_use]
    pub const fn front(&self) -> &Operations {
        &self.front
    }

    #[inline]
    pub fn set_back(&mut self, back: Operations) {
        self.back = back;
    }

    #[inline]
    pub fn set_front(&mut self, front: Operations) {
        self.front = front;
    }

    #[inline]
    pub fn set_test(&mut self, test: bool) {
        self.test = test;
    }

    #[inline]
    #[must_use]
    pub const fn test(&self) -> bool {
        self.test
    }
}

impl Default for Stencil {
    #[inline]
    fn default() -> Self {
        Self {
            test: false,
            front: Operations::default(),
            back: Operations::default(),
        }
    }
}
