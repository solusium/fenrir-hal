#![allow(
    clippy::ptr_as_ptr,
    clippy::cast_ptr_alignment,
    clippy::used_underscore_binding
)]

#[cfg(feature = "enum-derived")]
use enum_derived::Rand;
pub use guard::Guard;
use {
    crate::{
        device::stable_abi::Trait_TO,
        queue,
        render_pass::{self, RenderPass},
        texture::{self, Texture},
        version::Version,
        Extension,
    },
    abi_stable::{
        std_types::{
            RArc,
            RResult::{RErr, ROk},
            Tuple2,
        },
        StableAbi,
    },
    core::{
        fmt::{Debug, Formatter},
        marker::PhantomData,
        slice::from_raw_parts,
        str::from_utf8_unchecked,
    },
    fenrir_error::Error,
};

pub mod guard;

#[repr(C)]
#[derive(Clone, Copy, Debug, StableAbi)]
#[cfg_attr(feature = "enum-derived", derive(Rand))]
pub enum Type {
    Other,
    IntegratedGPU,
    DiscreteGPU,
    VirtualGPU,
    CPU,
}

pub mod stable_abi {
    #![allow(warnings)]

    use {
        crate::{
            device::{Guard, Handle, Type},
            queue, render_pass, texture,
            version::Version,
            Extension,
        },
        abi_stable::{
            sabi_trait,
            std_types::{RArc, RResult, RVec, Tuple2},
            StableAbi,
        },
        async_ffi::FfiFuture,
        core::marker::PhantomData,
        fenrir_error::Error,
    };

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub trait Trait: Clone + Debug + Eq + PartialEq + Send + Sync + 'static {
        fn device_type(&self) -> Type;

        fn driver_version(&self) -> RResult<Version, Error>;

        fn extensions(&self) -> RResult<RVec<Extension>, Error>;

        fn guard(&self) -> Guard;

        fn handle(&self) -> Handle;

        fn name(&self) -> Tuple2<*const u8, usize>;

        fn new_render_pass(
            &self,
            metadata: &render_pass::Metadata,
        ) -> RResult<crate::render_pass::stable_abi::Trait_TO<RArc<()>>, Error>;

        fn new_texture(
            &self,
            metadata: &texture::Metadata,
        ) -> FfiFuture<RResult<crate::texture::stable_abi::Trait_TO<RArc<()>>, Error>>;

        fn queue_families(&self) -> RVec<queue::Family>;
    }
}

#[repr(C)]
#[derive(Clone, Eq, PartialEq, StableAbi)]
pub struct Device {
    state: Trait_TO<RArc<()>>,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, StableAbi)]
pub struct Handle<'a> {
    data: Tuple2<*const u8, usize>,
    _phantom_data: PhantomData<&'a ()>,
}

impl Device {
    #[inline]
    #[must_use]
    pub const fn new(state: Trait_TO<RArc<()>>) -> Self {
        Self { state }
    }

    #[inline]
    #[must_use]
    pub fn device_type(&self) -> Type {
        self.state.device_type()
    }

    /// Get the driver version of this device.
    ///
    /// # Errors
    ///
    /// This function will return an error if the underlying API encounters an error.
    #[inline]
    pub fn driver_version(&self) -> Result<Version, Error> {
        self.state.driver_version().into()
    }

    /// Get the extension of the underlying API supported by this device.
    ///
    /// # Errors
    ///
    /// This function will return an error if the underlying API encounters an error.
    #[inline]
    pub fn extensions(&self) -> Result<Vec<Extension>, Error> {
        match self.state.extensions() {
            ROk(extensions) => Ok(extensions.into()),
            RErr(error) => Err(error),
        }
    }

    #[inline]
    #[must_use]
    pub fn handle(&self) -> Handle {
        self.state.handle()
    }

    #[must_use]
    pub fn guard(&self) -> guard::Guard {
        self.state.guard()
    }

    #[inline]
    #[must_use]
    pub fn name(&self) -> &str {
        let (bytes, size) = self.state.name().into();

        unsafe { from_utf8_unchecked(from_raw_parts(bytes, size)) }
    }

    /// Constructs a new render pass on this device.
    ///
    /// # Errors
    ///
    /// This function will return an error if the underlying API encounters an error.
    #[inline]
    pub fn new_render_pass(&self, metadata: &render_pass::Metadata) -> Result<RenderPass, Error> {
        match self.state.new_render_pass(metadata) {
            ROk(state) => Ok(RenderPass::new(state)),
            RErr(error) => Err(error),
        }
    }

    /// Constructs a new texture on this device.
    ///
    /// # Errors
    ///
    /// This function will return an error if the underlying API encounters an error.
    #[inline]
    pub async fn new_texture(&self, metadata: texture::Metadata) -> Result<Texture, Error> {
        match self.state.new_texture(&metadata).await {
            ROk(state) => Ok(Texture::new(state)),
            RErr(error) => Err(error),
        }
    }

    #[inline]
    #[must_use]
    pub fn queue_families(&self) -> Vec<queue::Family> {
        self.state.queue_families().into()
    }

    #[inline]
    #[must_use]
    pub fn state(&self) -> Trait_TO<RArc<()>> {
        self.state.clone()
    }
}

impl Handle<'_> {
    #[inline]
    #[must_use]
    pub fn new(data: &[u8]) -> Self {
        Handle {
            data: (data.as_ptr(), data.len()).into(),
            _phantom_data: PhantomData,
        }
    }

    #[inline]
    #[must_use]
    pub const fn data(&self) -> &[u8] {
        unsafe { from_raw_parts(self.data.0, self.data.1) }
    }
}

impl Debug for Device {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let state = &self.state;

        write!(f, "{state:?}")
    }
}
