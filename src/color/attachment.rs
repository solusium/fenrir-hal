use {
    crate::{
        clear::Value as ClearValue, color::blend::state::State as ColorBlendState,
        texture::view::View,
    },
    abi_stable::{std_types::ROption, StableAbi},
};

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum LoadOperation {
    Load,
    Clear,
    DontCare,
    None,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum StoreOperation {
    Store,
    DontCare,
    None,
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct Attachment {
    view: View,
    resolve_target: ROption<View>,
    load_operation: LoadOperation,
    store_operation: StoreOperation,
    clear_value: ClearValue,
    color_blend_state: ColorBlendState,
}

impl Attachment {
    #[inline]
    #[must_use]
    pub fn new(
        view: View,
        resolve_target: Option<View>,
        load_operation: LoadOperation,
        store_operation: StoreOperation,
        clear_value: ClearValue,
        color_blend_state: ColorBlendState,
    ) -> Self {
        let resolve_target = resolve_target.into();

        Self {
            view,
            resolve_target,
            load_operation,
            store_operation,
            clear_value,
            color_blend_state,
        }
    }

    #[inline]
    #[must_use]
    pub const fn clear_value(&self) -> crate::clear::Value {
        self.clear_value
    }

    #[inline]
    #[must_use]
    pub const fn color_blend_state(&self) -> &ColorBlendState {
        &self.color_blend_state
    }

    #[inline]
    #[must_use]
    pub const fn load_operation(&self) -> LoadOperation {
        self.load_operation
    }

    #[inline]
    #[must_use]
    pub fn resolve_target(&self) -> Option<&View> {
        self.resolve_target.as_ref().into()
    }

    #[inline]
    #[must_use]
    pub const fn store_operation(&self) -> StoreOperation {
        self.store_operation
    }

    #[inline]
    #[must_use]
    pub const fn view(&self) -> &View {
        &self.view
    }
}
