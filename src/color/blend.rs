use {
    crate::{color::RGBA, logical::Operation as LogicalOperation},
    abi_stable::{
        std_types::{ROption, RVec},
        StableAbi,
    },
    core::convert::Into,
    educe::Educe,
};

pub mod state;

pub type State = crate::color::blend::state::State;

#[repr(C)]
#[derive(Educe)]
#[educe(Default)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Factor {
    #[educe(Default)]
    Zero,
    One,
    SourceColor,
    OneMinusSourceColor,
    DestinationColor,
    OneMinusDestinationColor,
    SourceAlpha,
    OneMinusSourceAlpha,
    DestinationAlpha,
    OneMinusDestinationAlpha,
    ConstantColor,
    OneMinusConstantColor,
    ConstantAlpha,
    OneMinusConstantAlpha,
    SourceAlphaSaturate,
    Source1Color,
    OneMinusSource1Color,
    Source1Alpha,
    OneMinusSource1Alpha,
}

#[repr(C)]
#[derive(Educe)]
#[educe(Default)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Operation {
    #[educe(Default)]
    Add,
    Subtract,
    ReverseSubtract,
    Min,
    Max,
}

#[repr(C)]
#[derive(Educe)]
#[educe(Default)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Overlap {
    #[educe(Default)]
    Conjoint,
    Disjoint,
    Uncorrelated,
}

#[repr(C)]
#[derive(Educe)]
#[educe(Default)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub struct Advanced {
    clamp_results: bool,
    destination_premultiplied: bool,
    overlap: Overlap,
    source_premultiplied: bool,
}

#[repr(C)]
#[derive(Educe)]
#[educe(Default)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct Blend {
    advanced: ROption<RVec<Advanced>>,
    constants: RGBA<f32>,
    logical_operation: ROption<LogicalOperation>,
}

impl Advanced {
    #[inline]
    #[must_use]
    pub const fn new(
        clamp_results: bool,
        destination_premultiplied: bool,
        overlap: Overlap,
        source_premultiplied: bool,
    ) -> Self {
        Self {
            clamp_results,
            destination_premultiplied,
            overlap,
            source_premultiplied,
        }
    }

    #[inline]
    #[must_use]
    pub const fn clamp_results(&self) -> bool {
        self.clamp_results
    }

    #[inline]
    #[must_use]
    pub const fn destination_premultiplied(&self) -> bool {
        self.destination_premultiplied
    }

    #[inline]
    #[must_use]
    pub const fn overlap(&self) -> Overlap {
        self.overlap
    }

    #[inline]
    #[must_use]
    pub const fn source_premultiplied(&self) -> bool {
        self.source_premultiplied
    }
}

impl Blend {
    #[inline]
    #[must_use]
    pub fn new(
        advanced: Option<Vec<Advanced>>,
        constants: RGBA<f32>,
        logical_operation: Option<LogicalOperation>,
    ) -> Self {
        let advanced = advanced.map(Into::into).into();

        let logical_operation = logical_operation.into();

        Self {
            advanced,
            constants,
            logical_operation,
        }
    }

    #[inline]
    #[must_use]
    pub fn advanced(&self) -> Option<&[Advanced]> {
        self.advanced.as_ref().map(RVec::as_slice).into()
    }

    #[inline]
    #[must_use]
    pub const fn constants(&self) -> &RGBA<f32> {
        &self.constants
    }

    #[inline]
    #[must_use]
    pub fn logical_operation(&self) -> Option<LogicalOperation> {
        self.logical_operation.into()
    }
}
