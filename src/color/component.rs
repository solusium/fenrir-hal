use abi_stable::StableAbi;
#[cfg(feature = "enum-derived")]
use enum_derived::Rand;

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Component {
    R,
    G,
    B,
    A,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
#[cfg_attr(feature = "enum-derived", derive(Rand))]
pub enum Swizzle {
    Identity,
    Zero,
    One,
    R,
    G,
    B,
    A,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Default, Eq, PartialEq, StableAbi)]
pub struct Mapping {
    r: Swizzle,
    g: Swizzle,
    b: Swizzle,
    a: Swizzle,
}

impl Mapping {
    #[inline]
    #[must_use]
    pub const fn new(r: Swizzle, g: Swizzle, b: Swizzle, a: Swizzle) -> Self {
        Self { r, g, b, a }
    }

    #[inline]
    #[must_use]
    pub const fn a(&self) -> Swizzle {
        self.a
    }

    #[inline]
    #[must_use]
    pub const fn b(&self) -> Swizzle {
        self.b
    }

    #[inline]
    #[must_use]
    pub const fn g(&self) -> Swizzle {
        self.g
    }

    #[inline]
    #[must_use]
    pub const fn r(&self) -> Swizzle {
        self.r
    }
}

impl Default for Swizzle {
    #[inline]
    fn default() -> Self {
        Self::Identity
    }
}
