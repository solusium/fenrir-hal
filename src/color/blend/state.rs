use {
    crate::color::{
        blend::{Factor, Operation},
        Component,
    },
    abi_stable::{std_types::RVec, StableAbi},
    core::marker::PhantomData,
    educe::Educe,
};

pub struct Builder<'a> {
    enabled: bool,
    alpha_blend_operation: Operation,
    color_blend_operation: Operation,
    destination_alpha_blend_factor: Factor,
    destination_color_blend_factor: Factor,
    source_alpha_blend_factor: Factor,
    source_color_blend_factor: Factor,
    color_write_mask: Vec<Component>,
    _phantom_data: PhantomData<&'a ()>,
}

#[repr(C)]
#[derive(Educe)]
#[educe(Default)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct State {
    enabled: bool,
    alpha_blend_operation: Operation,
    color_blend_operation: Operation,
    destination_alpha_blend_factor: Factor,
    destination_color_blend_factor: Factor,
    source_alpha_blend_factor: Factor,
    source_color_blend_factor: Factor,
    color_write_mask: RVec<Component>,
}

impl Builder<'_> {
    #[inline]
    #[must_use]
    pub fn build(self) -> State {
        State {
            enabled: self.enabled,
            alpha_blend_operation: self.alpha_blend_operation,
            color_blend_operation: self.color_blend_operation,
            destination_alpha_blend_factor: self.destination_alpha_blend_factor,
            destination_color_blend_factor: self.destination_color_blend_factor,
            source_alpha_blend_factor: self.source_alpha_blend_factor,
            source_color_blend_factor: self.source_color_blend_factor,
            color_write_mask: self.color_write_mask.into(),
        }
    }

    #[inline]
    #[must_use]
    pub const fn alpha_blend_operation(mut self, alpha_blend_operation: Operation) -> Self {
        self.alpha_blend_operation = alpha_blend_operation;

        self
    }

    #[inline]
    #[must_use]
    pub const fn color_blend_operation(mut self, color_blend_operation: Operation) -> Self {
        self.color_blend_operation = color_blend_operation;

        self
    }

    #[inline]
    #[must_use]
    pub fn color_write_mask(mut self, color_write_mask: Vec<Component>) -> Self {
        self.color_write_mask = color_write_mask;

        self
    }

    #[inline]
    #[must_use]
    pub const fn destination_alpha_blend_factor(
        mut self,
        destination_alpha_blend_factor: Factor,
    ) -> Self {
        self.destination_alpha_blend_factor = destination_alpha_blend_factor;

        self
    }

    #[inline]
    #[must_use]
    pub const fn destination_color_blend_factor(
        mut self,
        destination_color_blend_factor: Factor,
    ) -> Self {
        self.destination_color_blend_factor = destination_color_blend_factor;

        self
    }

    #[inline]
    #[must_use]
    pub const fn enabled(mut self, enabled: bool) -> Self {
        self.enabled = enabled;

        self
    }

    #[inline]
    #[must_use]
    pub const fn source_alpha_blend_factor(mut self, source_alpha_blend_factor: Factor) -> Self {
        self.source_alpha_blend_factor = source_alpha_blend_factor;

        self
    }

    #[inline]
    #[must_use]
    pub const fn source_color_blend_factor(mut self, source_color_blend_factor: Factor) -> Self {
        self.source_color_blend_factor = source_color_blend_factor;

        self
    }
}

impl State {
    #[inline]
    #[must_use]
    pub const fn alpha_blend_operation(&self) -> Operation {
        self.alpha_blend_operation
    }

    #[inline]
    #[must_use]
    pub const fn builder<'a>() -> Builder<'a> {
        Builder {
            enabled: false,
            alpha_blend_operation: Operation::Add,
            color_blend_operation: Operation::Add,
            destination_alpha_blend_factor: Factor::One,
            destination_color_blend_factor: Factor::One,
            source_alpha_blend_factor: Factor::One,
            source_color_blend_factor: Factor::One,
            color_write_mask: Vec::new(),
            _phantom_data: PhantomData,
        }
    }

    #[inline]
    #[must_use]
    pub const fn color_blend_operation(&self) -> Operation {
        self.color_blend_operation
    }

    #[inline]
    #[must_use]
    pub const fn destination_alpha_blend_factor(&self) -> Factor {
        self.destination_alpha_blend_factor
    }

    #[inline]
    #[must_use]
    pub const fn destination_color_blend_factor(&self) -> Factor {
        self.destination_color_blend_factor
    }

    #[inline]
    #[must_use]
    pub const fn enabled(&self) -> bool {
        self.enabled
    }

    #[inline]
    #[must_use]
    pub const fn source_alpha_blend_factor(&self) -> Factor {
        self.source_alpha_blend_factor
    }

    #[inline]
    #[must_use]
    pub const fn source_color_blend_factor(&self) -> Factor {
        self.source_color_blend_factor
    }

    #[inline]
    #[must_use]
    pub fn color_write_mask(&self) -> &[Component] {
        &self.color_write_mask
    }
}
