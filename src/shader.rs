pub use crate::shader::{compiler::Compiler, module::Module, stage::Stage};

extern crate alloc;

pub mod compiler;
mod module;
mod stage;
