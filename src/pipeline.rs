use abi_stable::StableAbi;
#[cfg(feature = "enum-derived")]
use enum_derived::Rand;

#[repr(C)]
#[cfg_attr(feature = "enum-derived", derive(Rand))]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum BindPoint {
    Graphics,
    Compute,
    RayTracing,
}
