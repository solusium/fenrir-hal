use {
    crate::shader::compiler::stable_abi::{Trait, Trait_TO},
    abi_stable::{
        sabi_trait::TD_Opaque,
        std_types::{
            RArc,
            RResult::{self, RErr, ROk},
            RVec,
        },
        StableAbi,
    },
    async_ffi::{BorrowingFfiFuture, FutureExt},
    async_lock::Mutex,
    fenrir_error::{invalid_argument, state_not_recoverable},
    fenrir_shader_compiler::Metadata,
    itertools::Itertools,
    std::{
        fs::{read_dir, DirEntry},
        path::PathBuf,
    },
    strum::{EnumCount, EnumIter},
    tracing::error,
};

mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::non_send_fields_in_send_ty,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        non_local_definitions
    )]

    use {
        abi_stable::{
            sabi_trait,
            std_types::{RResult, RVec},
        },
        async_ffi::BorrowingFfiFuture,
        fenrir_shader_compiler::Metadata,
    };

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub(super) trait Trait: Clone + Debug + Send + Sync + 'static {
        fn compile<'a>(
            &'a self,
            metadata: Metadata<'a>,
        ) -> BorrowingFfiFuture<'a, RResult<RVec<u8>, fenrir_shader_compiler::Error>>;
    }
}

#[repr(C)]
#[derive(Clone, Copy, Debug, EnumCount, EnumIter, StableAbi)]
pub enum Backend {
    Naga,
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Compiler {
    inner: Trait_TO<RArc<()>>,
}

#[derive(Clone, Debug)]
#[allow(clippy::module_name_repetitions)]
pub enum CompilerOrSearchPath {
    SearchPaths(Vec<PathBuf>),
    Compiler(fenrir_shader_compiler::Compiler),
}

#[derive(Debug)]
struct Inner {
    compiler_or_search_path: Mutex<CompilerOrSearchPath>,
}

impl Compiler {
    /// Try to compile [`metadata.source()`](fenrir_shader_compiler::Metadata::source) into
    /// [`metadata.target_language()`](fenrir_shader_compiler::Metadata::target_language) code.
    ///
    /// # Errors
    ///
    /// If we constructed this Compiler from a vector of search paths and if we can't load a
    /// Compiler from any shared library in those search paths, this function will return
    /// [`StateNotRecoverable`](fenrir_error::Error::StateNotRecoverable).
    /// This function will return
    /// [`InvalidArgument`](fenrir_error::Error::InvalidArgument) if we couldn't read
    /// `metadata.source()` as code of
    /// [`metadata.source_language()`](crate::Metadata::source_language).
    /// If `metadata.source()` contains errors (e.g. syntax errors), we will return a vector of
    /// [`CompileErrors`](fenrir_shader_compiler::CompileError).
    #[inline]
    pub async fn compile(
        &self,
        metadata: Metadata<'_>,
    ) -> Result<Vec<u8>, fenrir_shader_compiler::Error> {
        match self.inner.compile(metadata).await {
            ROk(output) => Ok(output.into()),
            RErr(error) => Err(error),
        }
    }
}

impl Inner {
    #[inline]
    async fn compile(
        &self,
        metadata: Metadata<'_>,
    ) -> Result<Vec<u8>, fenrir_shader_compiler::Error> {
        let compiler = match self.get_compiler().await {
            Ok(compiler) => Ok(compiler),
            Err(error) => Err(fenrir_shader_compiler::Error::FenrirError(error)),
        }?;

        compiler.compile(metadata)
    }

    #[inline]
    async fn get_compiler(&self) -> Result<fenrir_shader_compiler::Compiler, fenrir_error::Error> {
        let mut compiler_or_search_path = self.compiler_or_search_path.lock().await;

        let (store_compiler, compiler) = match &*compiler_or_search_path {
            CompilerOrSearchPath::Compiler(compiler_) => Ok((false, compiler_.clone())),
            CompilerOrSearchPath::SearchPaths(search_paths) => {
                let maybe_compiler = search_paths
                    .iter()
                    .cartesian_product(["naga"])
                    .map(Self::load_compiler)
                    .filter_map(Result::ok)
                    .find(|_| true);

                maybe_compiler.map_or_else(
                    || {
                        Err(state_not_recoverable(&format!(
                        "couldn't load a compiler from a shared library in the given search paths: \
                        {search_paths:?}"
                    )))
                    },
                    |compiler_| Ok((true, compiler_)),
                )
            }
        }?;

        if store_compiler {
            *compiler_or_search_path = CompilerOrSearchPath::Compiler(compiler.clone());
        }

        drop(compiler_or_search_path);

        Ok(compiler)
    }

    #[inline]
    fn load_compiler(
        arguments: (&PathBuf, &str),
    ) -> Result<fenrir_shader_compiler::Compiler, fenrir_error::Error> {
        let (search_path, name) = arguments;

        let entries_result = read_dir(search_path);

        match entries_result {
            Err(error) => Err(invalid_argument(&format!(
                "couldn't load shader compiler from '{search_path:?}' {error}"
            ))),
            Ok(entries) => {
                let try_compiler_from_shared_library = |entry: DirEntry| {
                    let path = entry.path();

                    path.extension().map_or_else(
                        || None,
                        |extension| {
                            if extension == "so" || extension == "dll" || extension == "dylib" {
                                match fenrir_shader_compiler::Compiler::new(&path) {
                                    Ok(compiler) => Some(compiler),
                                    Err(error) => {
                                        error!(
                                            "failed to load \
                                            fenrir_shader_compiler::Compiler, \
                                            from '{path:?}' {error}"
                                        );

                                        None
                                    }
                                }
                            } else {
                                None
                            }
                        },
                    )
                };

                entries
                    .into_iter()
                    .filter_map(Result::ok)
                    .filter(|entry| entry.file_name().to_string_lossy().contains(name))
                    .filter_map(try_compiler_from_shared_library)
                    .find(|_| true)
                    .map_or_else(
                        || {
                            Err(invalid_argument(&format!(
                                "couldn't load shader compiler from '{search_path:?}': no suitable \
                                shared library found"
                            )))
                        },
                        Ok,
                    )
            }
        }
    }
}

impl From<fenrir_shader_compiler::Compiler> for Compiler {
    #[inline]
    fn from(value: fenrir_shader_compiler::Compiler) -> Self {
        let compiler_or_search_path = Mutex::new(CompilerOrSearchPath::Compiler(value));

        let inner = Trait_TO::from_ptr(
            RArc::new(Inner {
                compiler_or_search_path,
            }),
            TD_Opaque,
        );

        Self { inner }
    }
}

impl From<Vec<PathBuf>> for Compiler {
    #[inline]
    fn from(value: Vec<PathBuf>) -> Self {
        let compiler_or_search_path = Mutex::new(CompilerOrSearchPath::SearchPaths(value));

        let inner = Trait_TO::from_ptr(
            RArc::new(Inner {
                compiler_or_search_path,
            }),
            TD_Opaque,
        );

        Self { inner }
    }
}

impl Clone for Inner {
    #[inline]
    fn clone(&self) -> Self {
        unreachable!()
    }
}

impl Trait for Inner {
    #[inline]
    fn compile<'a>(
        &'a self,
        metadata: Metadata<'a>,
    ) -> BorrowingFfiFuture<'a, RResult<RVec<u8>, fenrir_shader_compiler::Error>> {
        async move {
            match self.compile(metadata).await {
                Ok(output) => ROk(output.into()),
                Err(error) => RErr(error),
            }
        }
        .into_ffi()
    }
}

unsafe impl Send for Inner {}
unsafe impl Sync for Inner {}
