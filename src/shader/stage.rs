use abi_stable::StableAbi;

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Stage {
    Compute,
    Fragment,
    Vertex,
}

impl From<fenrir_shader_compiler::Stage> for Stage {
    #[inline]
    fn from(stage: fenrir_shader_compiler::Stage) -> Self {
        match stage {
            fenrir_shader_compiler::Stage::Compute => Self::Compute,
            fenrir_shader_compiler::Stage::Fragment => Self::Fragment,
            fenrir_shader_compiler::Stage::Vertex => Self::Vertex,
        }
    }
}
