use {
    crate::shader::Stage,
    abi_stable::{
        marker_type::NonOwningPhantom,
        std_types::{RSlice, RStr, RString, RVec, Tuple2},
        StableAbi,
    },
    core::fmt::{Debug, Formatter},
    fenrir_shader_compiler::Language,
};

#[repr(C)]
#[derive(Clone, StableAbi)]
enum Code<'a> {
    Slice(RSlice<'a, u8>),
    Vector(Tuple2<RVec<u8>, NonOwningPhantom<RSlice<'a, u8>>>),
}

#[repr(C)]
#[derive(Clone, StableAbi)]
enum EntryPoint<'a> {
    Str(RStr<'a>),
    String(Tuple2<RString, NonOwningPhantom<RStr<'a>>>),
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct Module<'a, 'b> {
    stage: Stage,
    code: Code<'a>,
    language: Language,
    entry_point: EntryPoint<'b>,
}

impl<'a, 'b> Module<'a, 'b> {
    #[inline]
    #[must_use]
    pub const fn new(
        stage: Stage,
        code: &'a [u8],
        language: Language,
        entry_point: &'b str,
    ) -> Self {
        Self {
            stage,
            code: Code::Slice(RSlice::from_slice(code)),
            language,
            entry_point: EntryPoint::Str(RStr::from_str(entry_point)),
        }
    }

    #[inline]
    #[must_use]
    pub fn code(&self) -> &[u8] {
        match &self.code {
            Code::Slice(code) => code.as_slice(),
            Code::Vector(code) => &code.0,
        }
    }

    #[inline]
    #[must_use]
    pub fn entry_point(&self) -> &str {
        match &self.entry_point {
            EntryPoint::Str(code) => code.as_str(),
            EntryPoint::String(code) => &code.0,
        }
    }

    #[inline]
    #[must_use]
    pub const fn language(&self) -> Language {
        self.language
    }

    #[inline]
    #[must_use]
    pub const fn stage(&self) -> Stage {
        self.stage
    }

    #[inline]
    #[must_use]
    pub fn r#static(&self) -> Module<'static, 'static> {
        let code = match &self.code {
            Code::Slice(rslice) => rslice.to_vec(),
            Code::Vector(tuple2) => tuple2.0.clone().into(),
        };

        let entry_point = match &self.entry_point {
            EntryPoint::Str(rstr) => rstr.to_string(),
            EntryPoint::String(tuple2) => tuple2.0.clone().into(),
        };

        (self.stage, code, self.language, entry_point).into()
    }
}

impl Debug for Code<'_> {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        match self {
            Code::Slice(code) => write!(f, "{code:?}"),
            Code::Vector(code) => write!(f, "{:?}", code.0),
        }
    }
}

impl Eq for Code<'_> {}

impl PartialEq for Code<'_> {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Code::Slice(a), Code::Slice(b)) => a == b,
            (Code::Vector(a), Code::Vector(b)) => a.0 == b.0,
            _ => false,
        }
    }
}

impl Debug for EntryPoint<'_> {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        match self {
            EntryPoint::Str(entry_point) => write!(f, "{entry_point:?}"),
            EntryPoint::String(entry_point) => write!(f, "{:?}", entry_point.0),
        }
    }
}

impl Eq for EntryPoint<'_> {}

impl PartialEq for EntryPoint<'_> {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (EntryPoint::Str(a), EntryPoint::Str(b)) => a == b,
            (EntryPoint::String(a), EntryPoint::String(b)) => a.0 == b.0,
            _ => false,
        }
    }
}

impl<'a, 'b> From<(Stage, &'a [u8], Language, &'b str)> for Module<'a, 'b> {
    #[inline]
    fn from((stage, code, language, entry_point): (Stage, &'a [u8], Language, &'b str)) -> Self {
        Self {
            stage,
            code: Code::Slice(RSlice::from_slice(code)),
            language,
            entry_point: EntryPoint::Str(RStr::from_str(entry_point)),
        }
    }
}

impl<'a> From<(Stage, &'a [u8], Language, String)> for Module<'a, 'static> {
    #[inline]
    fn from((stage, code, language, entry_point): (Stage, &'a [u8], Language, String)) -> Self {
        Self {
            stage,
            code: Code::Slice(RSlice::from_slice(code)),
            language,
            entry_point: EntryPoint::String((entry_point.into(), NonOwningPhantom::NEW).into()),
        }
    }
}

impl<'b> From<(Stage, Vec<u8>, Language, &'b str)> for Module<'static, 'b> {
    #[inline]
    fn from((stage, code, language, entry_point): (Stage, Vec<u8>, Language, &'b str)) -> Self {
        Self {
            stage,
            code: Code::Vector((code.into(), NonOwningPhantom::NEW).into()),
            language,
            entry_point: EntryPoint::Str(RStr::from_str(entry_point)),
        }
    }
}

impl From<(Stage, Vec<u8>, Language, String)> for Module<'static, 'static> {
    #[inline]
    fn from((stage, code, language, entry_point): (Stage, Vec<u8>, Language, String)) -> Self {
        Self {
            stage,
            code: Code::Vector((code.into(), NonOwningPhantom::NEW).into()),
            language,
            entry_point: EntryPoint::String((entry_point.into(), NonOwningPhantom::NEW).into()),
        }
    }
}
