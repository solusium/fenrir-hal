#[allow(unused_imports, non_local_definitions)]
use fenrir_error::Error::StateNotRecoverable;
use {
    crate::{allocator, device::Device, instance::module::ModuleRef, shader::Compiler},
    abi_stable::{
        declare_root_module_statics,
        library::RootModule,
        package_version_strings,
        sabi_types::VersionStrings,
        std_types::{
            RArc,
            RResult::{RErr, ROk},
            RString,
        },
        DynTrait, StableAbi,
    },
    core::slice::from_raw_parts,
    fenrir_error::{state_not_recoverable, Error},
    sleipnir::ffi::runtime::Handle,
    std::path::Path,
    strum::EnumIter,
};

pub mod module {
    #![allow(
        clippy::must_use_candidate,
        clippy::expl_impl_clone_on_copy,
        clippy::trivially_copy_pass_by_ref,
        clippy::wrong_self_convention,
        improper_ctypes_definitions
    )]

    use {
        crate::instance::{Inner, Metadata},
        abi_stable::{
            std_types::{RArc, RResult, RVec, Tuple2},
            DynTrait, StableAbi,
        },
        fenrir_error::Error,
        sleipnir::ffi::runtime::Handle,
    };

    #[repr(C)]
    #[derive(StableAbi)]
    #[sabi(kind(Prefix(prefix_fields = ModulePrefix, prefix_ref = ModuleRef)))]
    pub struct Module {
        pub new: extern "C" fn(
            metadata: Metadata,
            runtime: Handle,
            allocator_manager: crate::allocator::manager::Manager,
        ) -> RResult<DynTrait<'static, RArc<()>, Inner>, Error>,

        pub devices:
            extern "C" fn(
                DynTrait<'static, RArc<()>, Inner>,
            )
                -> RResult<RVec<crate::device::stable_abi::Trait_TO<RArc<()>>>, Error>,

        #[sabi(last_prefix_field)]
        pub handle: extern "C" fn(
            DynTrait<'static, RArc<()>, Inner>,
        ) -> RResult<Tuple2<*const u8, usize>, Error>,
    }
}

#[repr(C)]
#[derive(Clone, Copy, Debug, EnumIter, StableAbi)]
pub enum Backend {
    Ash,
}

#[repr(C)]
#[derive(Clone, StableAbi)]
pub struct Instance {
    inner: DynTrait<'static, RArc<()>, Inner>,
    module: ModuleRef,
}

#[repr(C)]
#[derive(Clone, Debug, StableAbi)]
pub struct Metadata {
    pub name: RString,
    pub major: usize,
    pub minor: usize,
    pub patch: usize,
    pub tracing_level: u8,
    pub shader_compiler: Compiler,
}

#[repr(C)]
#[derive(StableAbi)]
#[sabi(impl_InterfaceType(Clone, Debug, Send))]
pub struct Inner;

impl Instance {
    /// Trying to load [`file`] and construct a new [`Instance`].
    ///
    /// # Errors
    ///
    /// This function will return an error if loading the file failed (with [`StateNotRecoverable`]
    /// and a string describing the exact error). See [`abi_stable::library::RootModule::load_from`]
    /// for more information.
    /// Other errors might occur when actually constructing the [`Backend`].
    pub fn new(
        file: &Path,
        metadata: Metadata,
        runtime: Handle,
        allocator_manager: allocator::manager::Manager,
    ) -> Result<Self, Error> {
        let module = match ModuleRef::load_from_file(file) {
            Ok(module_) => Ok(module_),
            Err(error) => Err(state_not_recoverable(&error.to_string())),
        }?;

        match module.new()(metadata, runtime, allocator_manager) {
            RErr(error) => Err(error),
            ROk(inner) => Ok(Self { inner, module }),
        }
    }

    /// Returns the devices of this [`Instance`].
    ///
    /// # Errors
    ///
    /// This function will return an error if any call of the underlying API returns an error.
    #[inline]
    pub fn devices(&self) -> Result<Vec<Device>, Error> {
        match self.module.devices()(self.inner.clone()) {
            RErr(error) => Err(error),
            ROk(states) => Ok(states.into_iter().map(Device::new).collect()),
        }
    }

    /// Returns the bytes of the handle of this [`Instance`].
    ///
    /// # Errors
    ///
    /// This function will return an error if any call of the underlying API returns an error.
    #[inline]
    pub fn handle(&self) -> Result<&[u8], Error> {
        match self.module.handle()(self.inner.clone()) {
            RErr(error) => Err(error),
            ROk(pointer_size_tuple) => {
                let (pointer, size) = pointer_size_tuple.into();
                Ok(unsafe { from_raw_parts(pointer, size) })
            }
        }
    }

    #[inline]
    #[must_use]
    pub const fn inner(&self) -> &DynTrait<'static, RArc<()>, Inner> {
        &self.inner
    }
}

unsafe impl Send for Instance {}

impl Default for Metadata {
    #[inline]
    fn default() -> Self {
        Self {
            name: String::new().into(),
            major: 0,
            minor: 0,
            patch: 0,
            tracing_level: 0,
            shader_compiler: Vec::new().into(),
        }
    }
}

#[allow(clippy::use_self)]
impl RootModule for ModuleRef {
    declare_root_module_statics! {ModuleRef}

    const BASE_NAME: &'static str = "fenrir_backend";

    const NAME: &'static str = "fenrir-backend";

    const VERSION_STRINGS: VersionStrings = package_version_strings!();
}
