use {
    abi_stable::{std_types::RVec, StableAbi},
    core::ops::Deref,
};

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
enum Inner<T, const N: usize> {
    Array([T; N]),
    Vec(RVec<T>),
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub(crate) struct SmallVec<T, const N: usize>(Inner<T, N>);

impl<T, const N: usize> SmallVec<T, N> {
    #[inline]
    pub(crate) fn new<'a>(values: &'a [T]) -> Self
    where
        T: Clone,
        [T; N]: TryFrom<&'a [T]>,
    {
        Self(values.try_into().map_or_else(
            |_| Inner::Vec(RVec::from_slice(values)),
            |array| Inner::Array(array),
        ))
    }
}

impl<T, const N: usize> Deref for SmallVec<T, N> {
    type Target = [T];

    #[inline]
    fn deref(&self) -> &Self::Target {
        match &self.0 {
            Inner::Array(array) => array,
            Inner::Vec(vector) => vector,
        }
    }
}
