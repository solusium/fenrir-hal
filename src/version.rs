use {
    abi_stable::StableAbi,
    core::fmt::{Debug, Formatter},
    smallvec::SmallVec,
};

const MAJOR_OFFSET: u32 = 22;
const MINOR_BYTE_MASK: usize = 0x3FF;
const MINOR_OFFSET: u32 = 12;
const PATCH_BYTE_MASK: usize = 0xFFF;

#[repr(C)]
#[derive(Clone, Copy, Eq, PartialEq, StableAbi)]
pub struct Version {
    data: u32,
}

impl Version {
    #[inline]
    #[must_use]
    pub fn new(major: u8, minor: u8, patch: u8) -> Self {
        let u32_version_components = [major, minor, patch]
            .into_iter()
            .map(u32::from)
            .collect::<SmallVec<[u32; 3]>>();

        let major_u32 = u32_version_components[0];
        let minor_u32 = u32_version_components[1];
        let patch_u32 = u32_version_components[2];

        Self {
            data: (major_u32 << MAJOR_OFFSET) | (minor_u32 << MINOR_OFFSET) | patch_u32,
        }
    }

    #[inline]
    #[must_use]
    pub const fn major(&self) -> usize {
        (self.data as usize) >> MAJOR_OFFSET
    }

    #[inline]
    #[must_use]
    pub const fn minor(&self) -> usize {
        ((self.data as usize) >> MINOR_OFFSET) & MINOR_BYTE_MASK
    }

    #[inline]
    #[must_use]
    pub const fn patch(&self) -> usize {
        (self.data as usize) & PATCH_BYTE_MASK
    }
}

impl Debug for Version {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let (major, minor, patch) = (self.major(), self.minor(), self.patch());

        writeln!(f, "{major:?}.{minor:?}.{patch:?}")
    }
}
