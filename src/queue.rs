pub mod family;

use {
    crate::queue::family::Flags,
    abi_stable::{std_types::Tuple2, StableAbi},
};

#[repr(C)]
#[derive(Debug, StableAbi)]
pub struct Family {
    pub index: usize,
    pub length: usize,
    pub flags: Tuple2<[Flags; 8], usize>,
}
