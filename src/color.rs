pub use crate::color::{attachment::Attachment, blend::Blend, component::Component};
use {abi_stable::StableAbi, educe::Educe, f32, float_eq::float_eq};

pub mod attachment;
pub mod blend;
pub mod component;

pub trait Arithmetic: Default {
    fn equal(&self, other: &Self) -> bool;
}

#[repr(C)]
#[derive(Educe)]
#[educe(Default)]
#[derive(Clone, Copy, Debug, StableAbi)]
pub struct RGBA<T>
where
    T: Arithmetic + Copy,
{
    r: T,
    g: T,
    b: T,
    a: T,
}

impl<T> RGBA<T>
where
    T: Arithmetic + Copy,
{
    pub const fn new(r: T, g: T, b: T, a: T) -> Self {
        Self { r, g, b, a }
    }

    #[inline]
    #[must_use]
    pub const fn a(&self) -> T {
        self.a
    }

    #[inline]
    #[must_use]
    pub const fn b(&self) -> T {
        self.b
    }

    #[inline]
    #[must_use]
    pub const fn g(&self) -> T {
        self.g
    }

    #[inline]
    #[must_use]
    pub const fn r(&self) -> T {
        self.r
    }
}

impl Arithmetic for f32 {
    #[inline]
    fn equal(&self, other: &Self) -> bool {
        float_eq!(self, other, r2nd <= Self::EPSILON)
    }
}

impl Arithmetic for isize {
    #[inline]
    fn equal(&self, other: &Self) -> bool {
        self == other
    }
}

impl Arithmetic for usize {
    #[inline]
    fn equal(&self, other: &Self) -> bool {
        self == other
    }
}

impl<T> Eq for RGBA<T> where T: Arithmetic + Copy {}

impl<T> PartialEq for RGBA<T>
where
    T: Arithmetic + Copy,
{
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.r.equal(&other.r)
            && self.g.equal(&other.g)
            && self.b.equal(&other.b)
            && self.a.equal(&other.a)
    }
}
