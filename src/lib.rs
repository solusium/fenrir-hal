#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#![allow(
    clippy::explicit_deref_methods,
    clippy::if_not_else,
    clippy::redundant_pub_crate,
    clippy::type_complexity
)]

pub mod allocation;
pub mod allocator;
pub mod clear;
pub mod color;
pub mod compare;
pub mod cull;
pub mod depth;
pub mod device;
pub mod format;
pub mod front;
pub mod geometry;
pub mod graphics;
pub mod instance;
pub mod logical;
mod multisample;
pub mod pipeline;
pub mod polygon;
pub mod primitive;
pub mod queue;
pub mod render_pass;
pub mod sample;
pub mod shader;
mod smallvec;
pub mod stencil;
pub mod texture;
pub mod version;
pub mod vertex;
mod viewport;

pub(crate) use crate::smallvec::SmallVec;
pub use crate::{
    depth::Depth, device::Device, instance::Instance, multisample::Multisample,
    primitive::Primitive, stencil::Stencil, viewport::Viewport,
};
use {
    crate::version::Version,
    abi_stable::{std_types::RString, StableAbi},
    core::fmt::Debug,
};

pub type RenderPass = crate::render_pass::RenderPass;

#[repr(C)]
#[derive(Debug, StableAbi)]
pub struct Extension {
    pub name: RString,
    pub version: Version,
}
