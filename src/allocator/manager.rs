use {
    crate::{
        allocator::{Allocator, ModuleRef},
        device::{Device, Guard as DeviceGuard},
    },
    abi_stable::{library::RootModule, std_types::RArc, DynTrait, StableAbi},
    educe::Educe,
    fenrir_error::{initialization_failed, Error},
    sleipnir::ffi::runtime::Handle,
    std::path::Path,
    tracing::error,
};

#[repr(C)]
#[derive(Clone, Educe, StableAbi)]
#[educe(Debug)]
pub struct Manager {
    #[educe(Debug(ignore))]
    module: ModuleRef,
    inner: DynTrait<'static, RArc<()>, Inner>,
}

#[repr(C)]
#[derive(StableAbi)]
#[sabi(impl_InterfaceType(Clone, Debug, Send, Sync))]
pub struct Inner;

impl Manager {
    /// Try to load an allocator manager from path.
    ///
    /// # Errors
    ///
    /// This function will return
    /// [`InitializationFailed`](fenrir_error::Error::InitializationFailed) if we cannot load an
    /// allocator manager from path.
    #[inline]
    pub fn new(path: &Path, runtime: Handle) -> Result<Self, Error> {
        match ModuleRef::load_from_file(path) {
            Ok(module) => {
                let inner = module.new()(runtime);

                Ok(Self { module, inner })
            }
            Err(error) => {
                let message = format!("failed to load allocator manager from {path:?}: {error:?}");

                error!(message);

                Err(initialization_failed(&message))
            }
        }
    }

    /// Try to construct an allocator for device from this manager.
    ///
    /// # Errors
    ///
    /// This function will return an error if any call of the underlying API returns an error.
    #[inline]
    pub fn allocator(&self, device: &Device) -> Result<Allocator, Error> {
        self.allocator_form_guard(device.guard())
    }

    /// Try to construct an allocator for the device guard from this manager.
    ///
    /// # Errors
    ///
    /// This function will return an error if any call of the underlying API returns an error.
    #[inline]
    pub fn allocator_form_guard(&self, device: DeviceGuard) -> Result<Allocator, Error> {
        Allocator::new(self.module, &self.inner, device)
    }
}

unsafe impl Send for Manager {}
unsafe impl Sync for Manager {}
