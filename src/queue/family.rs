use abi_stable::StableAbi;
#[cfg(feature = "enum-derived")]
use enum_derived::Rand;

#[repr(C)]
#[cfg_attr(feature = "enum-derived", derive(Rand))]
#[derive(Clone, Copy, Debug, PartialEq, Eq, StableAbi)]
pub enum Flags {
    Graphics,
    Compute,
    Transfer,
    SparseBinding,
    ProtectedMemory,
    VideoDecode,
    VideoEncode,
    OpticalFlow,
    Unknown,
}
