use {abi_stable::StableAbi, float_eq::float_eq};

#[repr(C)]
#[derive(Clone, Copy, Debug, StableAbi)]
pub struct Viewport {
    x: f32,
    y: f32,
    width: f32,
    height: f32,
    min_depth: f32,
    max_depth: f32,
}

impl Viewport {
    #[inline]
    #[must_use]
    pub const fn new(
        x: f32,
        y: f32,
        width: f32,
        height: f32,
        min_depth: f32,
        max_depth: f32,
    ) -> Self {
        Self {
            x,
            y,
            width,
            height,
            min_depth,
            max_depth,
        }
    }

    #[inline]
    #[must_use]
    pub const fn x(self) -> f32 {
        self.x
    }

    #[inline]
    #[must_use]
    pub const fn y(self) -> f32 {
        self.y
    }

    #[inline]
    #[must_use]
    pub const fn width(self) -> f32 {
        self.width
    }

    #[inline]
    #[must_use]
    pub const fn height(self) -> f32 {
        self.height
    }

    #[inline]
    #[must_use]
    pub const fn min_depth(self) -> f32 {
        self.min_depth
    }

    #[inline]
    #[must_use]
    pub const fn max_depth(self) -> f32 {
        self.max_depth
    }
}

impl Eq for Viewport {}

impl PartialEq for Viewport {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        float_eq!(self.x, other.x, r2nd <= f32::EPSILON)
            && float_eq!(self.y, other.y, r2nd <= f32::EPSILON)
            && float_eq!(self.width, other.width, r2nd <= f32::EPSILON)
            && float_eq!(self.height, other.height, r2nd <= f32::EPSILON)
            && float_eq!(self.min_depth, other.min_depth, r2nd <= f32::EPSILON)
            && float_eq!(self.max_depth, other.max_depth, r2nd <= f32::EPSILON)
    }
}
