use abi_stable::StableAbi;

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Format {
    X32Sfloat,
    X32Y32Sfloat,
    X32Y32Z32Sfloat,
    X32Y32Z32W32Sfloat,
}
