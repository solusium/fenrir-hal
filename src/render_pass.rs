use {
    crate::{
        color::Attachment as ColorAttachment,
        device::Device,
        geometry::Rect2D,
        graphics::{pipeline::Metadata as GraphicsPipelineMetadata, Pipeline},
        pipeline::BindPoint,
        render_pass::stable_abi::Trait_TO,
    },
    abi_stable::{
        std_types::{RArc, RVec},
        StableAbi,
    },
    fenrir_error::Error,
    fenrir_shader_compiler::Error as CompilerError,
};

pub mod stable_abi {
    #![allow(
        clippy::cast_ptr_alignment,
        clippy::non_send_fields_in_send_ty,
        clippy::ptr_as_ptr,
        clippy::used_underscore_binding,
        improper_ctypes_definitions,
        non_local_definitions
    )]

    use {
        crate::{
            color::Attachment as ColorAttachment,
            graphics::{pipeline::Metadata as GraphicsPipelineMetadata, Pipeline},
            render_pass::Metadata,
        },
        abi_stable::{sabi_trait, std_types::RResult},
        async_ffi::BorrowingFfiFuture,
        fenrir_error::Error,
        fenrir_shader_compiler::Error as CompilerError,
    };

    #[sabi_trait]
    #[sabi(use_dyntrait)]
    pub trait Trait: Clone + Debug + Eq + PartialEq + Send + Sync + 'static {
        fn add_color_attachment<'a>(
            &'a self,
            color_attachment: ColorAttachment,
        ) -> BorrowingFfiFuture<'a, RResult<(), Error>>;

        fn metadata<'a>(&'a self) -> BorrowingFfiFuture<'a, Metadata>;

        fn new_graphics_pipeline<'a>(
            &'a self,
            graphics_pipeline_metadata: GraphicsPipelineMetadata<'a, 'a>,
        ) -> BorrowingFfiFuture<'a, RResult<Pipeline, CompilerError>>;

        fn remove_color_attachment<'a>(
            &'a self,
            color_attachment: ColorAttachment,
        ) -> BorrowingFfiFuture<'a, RResult<(), Error>>;

        fn render<'a>(&'a self) -> BorrowingFfiFuture<'a, RResult<(), Error>>;
    }
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct Metadata {
    device: Device,
    color_attachments: RVec<ColorAttachment>,
    render_area: Rect2D,
    layer_count: usize,
    view_mask: usize,
    bind_point: BindPoint,
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct RenderPass {
    inner: Trait_TO<RArc<()>>,
}

impl Metadata {
    #[inline]
    #[must_use]
    pub fn new(
        device: Device,
        color_attachments: Vec<ColorAttachment>,
        render_area: Rect2D,
        layer_count: usize,
        view_mask: usize,
        bind_point: BindPoint,
    ) -> Self {
        let color_attachments = color_attachments.into();

        Self {
            device,
            color_attachments,
            render_area,
            layer_count,
            view_mask,
            bind_point,
        }
    }

    #[inline]
    #[must_use]
    pub const fn bind_point(&self) -> BindPoint {
        self.bind_point
    }

    #[inline]
    #[must_use]
    pub fn color_attachments(&self) -> &[ColorAttachment] {
        &self.color_attachments
    }

    #[inline]
    #[must_use]
    pub fn device(&self) -> Device {
        self.device.clone()
    }

    #[inline]
    #[must_use]
    pub const fn layer_count(&self) -> usize {
        self.layer_count
    }

    #[inline]
    #[must_use]
    pub const fn render_area(&self) -> Rect2D {
        self.render_area
    }

    #[inline]
    #[must_use]
    pub const fn view_mask(&self) -> usize {
        self.view_mask
    }
}

impl RenderPass {
    #[inline]
    pub const fn new(inner: Trait_TO<RArc<()>>) -> Self {
        Self { inner }
    }

    #[inline]
    pub async fn add_color_attachment(
        &self,
        color_attachment: ColorAttachment,
    ) -> Result<(), Error> {
        self.inner
            .add_color_attachment(color_attachment)
            .await
            .into()
    }

    #[inline]
    #[must_use]
    pub fn state(&self) -> Trait_TO<RArc<()>> {
        self.inner.clone()
    }

    #[inline]
    #[must_use]
    pub async fn metadata(&self) -> Metadata {
        self.inner.metadata().await
    }

    /// Construct a new pipeline used by this render pass.
    ///
    /// # Errors
    ///
    /// This function will return an error if any call of the underlying API returns an error.
    #[inline]
    pub async fn new_graphics_pipeline<'a>(
        &'a self,
        metadata: GraphicsPipelineMetadata<'a, 'a>,
    ) -> Result<Pipeline, CompilerError> {
        self.inner.new_graphics_pipeline(metadata).await.into()
    }

    #[inline]
    pub async fn remove_color_attachment(
        &self,
        color_attachment: ColorAttachment,
    ) -> Result<(), Error> {
        self.inner
            .remove_color_attachment(color_attachment)
            .await
            .into()
    }

    /// Execute the render pass.
    ///
    /// # Errors
    ///
    /// This function will return an error if any call of the underlying API returns an error.
    #[inline]
    pub async fn render(&self) -> Result<(), Error> {
        self.inner.render().await.into()
    }
}
