use {
    crate::{format::Format, SmallVec},
    abi_stable::StableAbi,
    core::fmt::Debug,
};

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Rate {
    Instance,
    Vertex,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub struct Attribute {
    location: usize,
    binding: usize,
    format: Format,
    offset: usize,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub struct Binding {
    index: usize,
    stride: usize,
    rate: Rate,
    divisor: usize,
}

#[repr(C)]
#[derive(Clone, Debug, Eq, PartialEq, StableAbi)]
pub struct Input {
    attributes: SmallVec<Attribute, 3>,
    bindings: SmallVec<Binding, 3>,
}

impl Attribute {
    #[inline]
    #[must_use]
    pub const fn new(location: usize, binding: usize, format: Format, offset: usize) -> Self {
        Self {
            location,
            binding,
            format,
            offset,
        }
    }

    #[inline]
    #[must_use]
    pub const fn binding(&self) -> usize {
        self.binding
    }

    #[inline]
    #[must_use]
    pub const fn format(&self) -> Format {
        self.format
    }

    #[inline]
    #[must_use]
    pub const fn location(&self) -> usize {
        self.location
    }

    #[inline]
    #[must_use]
    pub const fn offset(&self) -> usize {
        self.offset
    }
}

impl Binding {
    #[inline]
    #[must_use]
    pub const fn new(index: usize, stride: usize, rate: Rate, divisor: usize) -> Self {
        Self {
            index,
            stride,
            rate,
            divisor,
        }
    }

    #[inline]
    #[must_use]
    pub const fn divisor(&self) -> usize {
        self.divisor
    }

    #[inline]
    #[must_use]
    pub const fn index(&self) -> usize {
        self.index
    }

    #[inline]
    #[must_use]
    pub const fn rate(&self) -> Rate {
        self.rate
    }

    #[inline]
    #[must_use]
    pub const fn stride(&self) -> usize {
        self.stride
    }
}

impl Input {
    #[inline]
    #[must_use]
    pub fn new(attributes: &[Attribute], bindings: &[Binding]) -> Self {
        let attributes = SmallVec::new(attributes);
        let bindings = SmallVec::new(bindings);

        Self {
            attributes,
            bindings,
        }
    }

    #[inline]
    #[must_use]
    pub fn attributes(&self) -> &[Attribute] {
        &self.attributes
    }

    #[inline]
    #[must_use]
    pub fn bindings(&self) -> &[Binding] {
        &self.bindings
    }
}

impl Default for Input {
    #[inline]
    fn default() -> Self {
        Self::new(&[], &[])
    }
}
