use {abi_stable::StableAbi, educe::Educe};

#[repr(C)]
#[derive(Educe)]
#[educe(Default)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Operation {
    #[educe(Default)]
    Clear,
    And,
    AndReverse,
    Copy,
    AndInverted,
    NoOp,
    Xor,
    Or,
    Not,
    Nor,
    Equivalent,
    Invert,
    OrReverse,
    CopyInverted,
    OrInverted,
    Nand,
    Set,
}
