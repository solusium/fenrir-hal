use abi_stable::StableAbi;

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub enum Topology {
    PointList,
    LineList,
    LineStrip,
    TriangleList,
    TriangleStrip,
    TriangleFan,
    LineListWithAdjacency,
    LineStripWithAdjacency,
    TriangleListWithAdjacency,
    TriangleStripWithAdjacency,
    PatchList,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, StableAbi)]
pub struct Primitive {
    topology: Topology,
    restart: bool,
}

impl Primitive {
    #[inline]
    #[must_use]
    pub const fn new(topology: Topology, restart: bool) -> Self {
        Self { topology, restart }
    }

    #[inline]
    #[must_use]
    pub const fn restart(&self) -> bool {
        self.restart
    }

    #[inline]
    pub fn set_restart(&mut self, restart: bool) {
        self.restart = restart;
    }

    #[inline]
    pub fn set_topology(&mut self, topology: Topology) {
        self.topology = topology;
    }

    #[inline]
    #[must_use]
    pub const fn topology(&self) -> Topology {
        self.topology
    }
}

impl Default for Primitive {
    #[inline]
    fn default() -> Self {
        Self::new(Topology::TriangleList, false)
    }
}
