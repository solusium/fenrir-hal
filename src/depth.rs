pub use crate::depth::bias::Bias;
use {crate::compare::Operation, abi_stable::StableAbi, float_eq::float_eq};

mod bias;

#[repr(C)]
#[derive(Clone, Copy, Debug, StableAbi)]
pub struct Depth {
    test: bool,
    write: bool,
    compare_operation: Operation,
    bounds_test: bool,
    min_bounds: f32,
    max_bounds: f32,
}

impl Depth {
    #[inline]
    #[must_use]
    pub const fn new(
        test: bool,
        write: bool,
        compare_operation: Operation,
        bounds_test: bool,
        min_bounds: f32,
        max_bounds: f32,
    ) -> Self {
        Self {
            test,
            write,
            compare_operation,
            bounds_test,
            min_bounds,
            max_bounds,
        }
    }

    #[inline]
    #[must_use]
    pub const fn bounds_test(&self) -> bool {
        self.bounds_test
    }

    #[inline]
    #[must_use]
    pub const fn compare_operation(&self) -> Operation {
        self.compare_operation
    }

    #[inline]
    #[must_use]
    pub const fn max_bounds(&self) -> f32 {
        self.max_bounds
    }

    #[inline]
    #[must_use]
    pub const fn min_bounds(&self) -> f32 {
        self.min_bounds
    }

    #[inline]
    pub fn set_bounds_test(&mut self, bounds_test: bool) {
        self.bounds_test = bounds_test;
    }

    #[inline]
    pub fn set_compare_operation(&mut self, compare_operation: Operation) {
        self.compare_operation = compare_operation;
    }

    #[inline]
    pub fn set_max_bounds(&mut self, max_bounds: f32) {
        self.max_bounds = max_bounds;
    }

    #[inline]
    pub fn set_min_bounds(&mut self, min_bounds: f32) {
        self.min_bounds = min_bounds;
    }

    #[inline]
    pub fn set_test(&mut self, test: bool) {
        self.test = test;
    }

    #[inline]
    pub fn set_write(&mut self, write: bool) {
        self.write = write;
    }

    #[inline]
    #[must_use]
    pub const fn test(&self) -> bool {
        self.test
    }

    #[inline]
    #[must_use]
    pub const fn write(&self) -> bool {
        self.write
    }
}

impl Default for Depth {
    #[inline]
    fn default() -> Self {
        Self {
            test: false,
            write: false,
            compare_operation: Operation::Never,
            bounds_test: false,
            min_bounds: 0.0,
            max_bounds: 0.0,
        }
    }
}

impl Eq for Depth {}

impl PartialEq for Depth {
    #[inline]
    #[must_use]
    fn eq(&self, other: &Self) -> bool {
        self.test == other.test
            && self.write == other.write
            && self.compare_operation == other.compare_operation
            && self.bounds_test == other.bounds_test
            && float_eq!(self.min_bounds, other.min_bounds, r2nd <= f32::EPSILON)
            && float_eq!(self.max_bounds, other.max_bounds, r2nd <= f32::EPSILON)
    }
}
